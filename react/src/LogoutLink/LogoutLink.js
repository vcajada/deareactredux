import React          from 'react';
import { NavLink }    from 'reactstrap';

import history        from '../lib/history';
import { auth }       from '../lib/auth';

import './LogoutLink.css';

const LogoutLink = (props) => (
    <NavLink className="logout-link header-link" onClick={() => {
        auth.signout(() => {
            if (typeof props.dapId === 'undefined') history.push('/notfound');
            else {
                history.push('/' + props.dapId + '/login');
                localStorage.removeItem('token');
            }
        });
    }}>Logout</NavLink>
);

export default LogoutLink
