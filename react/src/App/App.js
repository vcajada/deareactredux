import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import PrivateRoute from   '../PrivateRoute/PrivateRoute';
import Login from          '../Login/Login';
import EmptyPage from      '../EmptyPage/EmptyPage';
import Dashboard from      '../Dashboard/Dashboard';
import NotFound from       '../NotFound/NotFound';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faPencilAlt,
    faTrashAlt,
    faChartArea,
    faChartPie,
    faTachometerAlt,
    faNewspaper,
    faHome,
    faCog,
    faPowerOff,
    faToggleOff,
    faToggleOn,
    faMapMarked,
    faMapMarker,
    faSitemap
} from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';

library.add(
    faPencilAlt,
    faTrashAlt,
    faChartArea,
    faChartPie,
    faNewspaper,
    faHome,
    faCog,
    faTachometerAlt,
    faPowerOff,
    faToggleOff,
    faToggleOn,
    faMapMarked,
    faMapMarker,
    faSitemap,
    fab
);

class App extends Component {

    render() {
        return (
            <Switch>
                <Route path="/:dap_id/login" component={Login} />
                <PrivateRoute exact path="/:dap_id/" component={EmptyPage} />
                <PrivateRoute path="/:dap_id/dashboard/:dpg_id" component={Dashboard} />
                <PrivateRoute component={NotFound} />
            </Switch>
        );
    }
}

export default App;
