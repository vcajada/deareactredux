import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import {
    Navbar,
    NavbarToggler,
    Collapse,
    Nav,
    NavLink,
    NavItem
} from 'reactstrap';

import './Toolbar.css';

class Toolbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        }

        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {

        return (
            <div>
                <Navbar expand="md" light className="toolbar-container">
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            {/*<NavItem className="toolbar-item" title="Create Map widget" onClick={() => this.props.addWidget('DWG_sitemap')}>
                                <FontAwesomeIcon icon="sitemap" />
                            </NavItem>*/}
                            <NavItem className="toolbar-item hidden-xs" title="Create Map widget" onClick={() => this.props.addWidget('DWG_map')}>
                                <FontAwesomeIcon icon="map-marked" />
                            </NavItem>
                            <NavItem className="toolbar-item hidden-xs" title="Create Status widget" onClick={() => this.props.addWidget('DWG_status')}>
                                <FontAwesomeIcon icon="power-off" />
                            </NavItem>
                            <NavItem className="toolbar-item hidden-xs" title="Create Value widget" onClick={() => this.props.addWidget('DWG_value')}>
                                <FontAwesomeIcon icon="newspaper" />
                            </NavItem>
                            <NavItem className="toolbar-item hidden-xs" title="Create HTML widget" onClick={() => this.props.addWidget('DWG_html')}>
                                <FontAwesomeIcon icon={['fab', 'html5']} />
                            </NavItem>
                            <NavItem className="toolbar-separator hidden-xs">
                                |
                            </NavItem>
                            <NavItem className="toolbar-item hidden-xs" title="Create Gauge widget" onClick={() => this.props.addWidget('DWG_gauge')}>
                                <FontAwesomeIcon icon="tachometer-alt" />
                            </NavItem>
                            <NavItem className="toolbar-item hidden-xs" title="Create Pie Chart widget" onClick={() => this.props.addWidget('DWG_piechart')}>
                                <FontAwesomeIcon icon="chart-pie" />
                            </NavItem>
                            <NavItem className="toolbar-item hidden-xs" title="Create Bar Chart widget" onClick={() => this.props.addWidget('DWG_areachart')}>
                                <FontAwesomeIcon icon="chart-area" />
                            </NavItem>
                            <NavItem className="visible-xs" onClick={() => this.props.addWidget('DWG_map')}>
                                <NavLink>Create Map widget</NavLink>
                            </NavItem>
                            <NavItem className="visible-xs" onClick={() => this.props.addWidget('DWG_status')}>
                                <NavLink>Create Status widget</NavLink>
                            </NavItem>
                            <NavItem className="visible-xs" onClick={() => this.props.addWidget('DWG_value')}>
                                <NavLink>Create Value widget</NavLink>
                            </NavItem>
                            <NavItem className="visible-xs" onClick={() => this.props.addWidget('DWG_html')}>
                                <NavLink>Create HTML widget</NavLink>
                            </NavItem>
                            <NavItem className="visible-xs" onClick={() => this.props.addWidget('DWG_gauge')}>
                                <NavLink>Create Gauge Widget</NavLink>
                            </NavItem>
                            <NavItem className="visible-xs" onClick={() => this.props.addWidget('DWG_piechart')}>
                                <NavLink>Create Pie Chart widget</NavLink>
                            </NavItem>
                            <NavItem className="visible-xs" onClick={() => this.props.addWidget('DWG_areachart')}>
                                <NavLink>Create Bar Chart widget</NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

export default Toolbar;
