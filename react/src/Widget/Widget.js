import React, { Component } from 'react';
import { withSize } from 'react-sizeme';
import { CardBody } from 'reactstrap';

import { request } from '../lib/request.js';

import Header from './Header';
import Edit from './Edit/Edit';
import Configure from './Configure/Configure';
import Html from './Html/Html';
import Value from './Value/Value';
import Areachart from './Areachart/Areachart';
import Piechart from './Piechart/Piechart';
import Gauge from './Gauge/Gauge';
import Status from './Status/Status';
import Map from './Map/Map';
import Sitemap from './Sitemap/Sitemap';

import './Widget.css';

class Widget extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalEdit: false,
            modalConfigure: false,
            headerSize: {width: 0, height: 0}
        }

        this.delete = this.delete.bind(this);
        this.toggleModalEdit = this.toggleModalEdit.bind(this);
        this.toggleModalConfigure = this.toggleModalConfigure.bind(this);
        this.setHeaderSize = this.setHeaderSize.bind(this);
    }

    toggleModalEdit() {
        this.setState({modalEdit: !this.state.modalEdit});
    }

    toggleModalConfigure() {
        this.setState({modalConfigure: !this.state.modalConfigure});
    }

    delete() {
        request.delete(
            'widget/' + this.props.widget.DWG_ID,
            {},
            () => this.props.getWidgets(),
            () => console.log('delete widget failed.')
        );
    }

    setHeaderSize(size) {
        this.setState({headerSize: size});
    }

    render() {
        const widgetSize = { width: this.props.size.width - 40/*card body padding... TODO*/, height: (this.props.size.height - this.state.headerSize.height) }
        return (
            <div className="widget">
                <CardBody className="widget-content">
                    <Header
                        toggleModalConfigure={this.toggleModalConfigure}
                        toggleModalEdit={this.toggleModalEdit}
                        delete={this.delete}
                        widget={this.props.widget}
                        setSize={this.setHeaderSize}
                    />
                    {this.props.widget.DWG_TYPE === 'DWG_html' &&
                        <Html
                            widget={this.props.widget}
                        />}
                    {this.props.widget.DWG_TYPE === 'DWG_value' &&
                        <Value
                            widget={this.props.widget}
                            size={widgetSize}
                        />}
                    {this.props.widget.DWG_TYPE === 'DWG_areachart' &&
                        <Areachart
                            widget={this.props.widget}
                            layouts={this.props.layouts}
                            size={widgetSize}
                        />}
                    {this.props.widget.DWG_TYPE === 'DWG_piechart' &&
                        <Piechart
                            widget={this.props.widget}
                            layouts={this.props.layouts}
                            size={widgetSize}
                        />}
                    {this.props.widget.DWG_TYPE === 'DWG_gauge' &&
                        <Gauge
                            widget={this.props.widget}
                            layouts={this.props.layouts}
                            size={widgetSize}
                        />}
                    {this.props.widget.DWG_TYPE === 'DWG_status' &&
                        <Status
                            widget={this.props.widget}
                            layouts={this.props.layouts}
                            size={widgetSize}
                        />}
                    {this.props.widget.DWG_TYPE === 'DWG_map' &&
                        <Map
                            widget={this.props.widget}
                            layouts={this.props.layouts}
                            size={widgetSize}
                        />}
                    {this.props.widget.DWG_TYPE === 'DWG_sitemap' &&
                        <Sitemap
                            widget={this.props.widget}
                            layouts={this.props.layouts}
                            size={widgetSize}
                        />}
                </CardBody>
                <Edit
                    widget={this.props.widget}
                    modal={this.state.modalEdit}
                    getWidgets={this.props.getWidgets}
                    toggleModal={this.toggleModalEdit}
                    dpgId={this.props.dpgId}
                />
                <Configure
                    widget={this.props.widget}
                    modal={this.state.modalConfigure}
                    getWidgets={this.props.getWidgets}
                    toggleModal={this.toggleModalConfigure}
                    dpgId={this.props.dpgId}
                />
            </div>
        );
    }
}

export default withSize({monitorHeight: true})(Widget);
