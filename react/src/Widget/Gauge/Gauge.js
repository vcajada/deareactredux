import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';
import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import SolidGauge from 'highcharts/modules/solid-gauge';

import { request } from '../../lib/request.js';
import { utils } from '../../lib/utils.js';

class Gauge extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            config: {},
            options: this.getDefaultOptions()
        };
    }

    componentDidMount() {
        if (!this.props.widget.CFG_ID) { return; }
        utils.setRefresh(this.props.widget.DWG_REFRESH_FREQ, this.updateGraph);
        this.updateGraph();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.widget.CFG_ID !== this.props.widget.CFG_ID) { this.updateGraph(); }
        if (utils.didWidgetResize(prevProps, this.props)) { this.refs.chart.getChart().reflow(); }
    }

    updateGraph() {

        let configId = this.props.widget.CFG_ID;
        let dataLoaded = false;
        let configLoaded = false;

        request.get(
            'data/' + configId,
            {
                data_type: 'last_data'
            },
            // success callback
            (data) => {
                this.setState({
                    data: data[0]
                }, () => {
                    // If config finished loading, then render, if not, let config do the render
                    if (configLoaded) {
                        this.setGaugeOptions();
                    }
                });
            },
            // fail callback
            () => console.log('load data failed'),
            () => dataLoaded = true
        );
        request.get(
            'config/' + configId,
            {},
            (config) => {
                this.setState({
                    config: config[configId]
                }, () => {
                    if (dataLoaded) {
                        this.setGaugeOptions();
                    }
                });
            },
            () => console.log('load config failed'),
            () => configLoaded = true
        )
    }

    setGaugeOptions() {
        let series = [];

        if (!this.state.data) {
            series = [{
                data: [0]
            }];
        }

        series = [{
            name: this.state.config.label_D1,
            data: [this.state.data.data],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">' + this.state.config.unite_D1 + '</span></div>'
            },
            tooltip: {
                valueSuffix: ' ' + this.state.config.unite_D1
            }
        }];

        let options = this.getDefaultOptions();
        options.series = series;
        this.setState({options: options});
    }

    getDefaultOptions() {

        return {
            chart: {
                type: 'solidgauge'
            },
            title: null,
            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            tooltip: {
                enabled: false
            },
            // the value axis
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                },
                min: 0,
                max: 200,
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            },
            credits: {
                enabled: false
            }
        };
    }

    render() {

        HighchartsMore(ReactHighcharts.Highcharts);
        SolidGauge(ReactHighcharts.Highcharts);

        return (
            <div className="highcharts-wrapper" style={{height: `${this.props.size.height}px`}}>
                <ReactHighcharts
                    isPureConfig
                    config={this.state.options}
                    ref="chart"
                />
            </div>
        )
    }
}
export default Gauge;
