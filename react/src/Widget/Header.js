import React, { Component } from 'react';
import { withSize } from 'react-sizeme';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { CardTitle } from 'reactstrap';

class Header extends Component {
    componentDidMount() {
        this.props.setSize(this.props.size);
    }

    render() {
        return (
            <div>
                <div className="widget-config">
                    <FontAwesomeIcon className="widget-icon" icon="cog" title="Configure widget" onClick={this.props.toggleModalConfigure} />
                    <FontAwesomeIcon className="widget-icon" icon="pencil-alt" title="Edit widget" onClick={this.props.toggleModalEdit} />
                    <FontAwesomeIcon className="widget-icon" icon="trash-alt" title="Delete widget" onClick={this.props.delete} />
                </div>
                <CardTitle className="widget-title">{this.props.widget.DWG_LABEL}</CardTitle>
            </div>
        );
    }
}

export default withSize({monitorHeight: true})(Header);
