import React, { Component } from 'react';
import { Form, FormGroup, Input, Row, Col, Label, FormFeedback, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { request } from '../../lib/request';

class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            label: this.props.widget.DWG_LABEL,
            type:  this.props.widget.DWG_TYPE.replace('DWG_', ''),
            refreshFreq: 0
        };

        this.edit = this.edit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    edit() {
        let params = {
            label: this.state.label,
            refresh_freq: this.state.refreshFreq,
            page_id: this.props.dpgId,
        };

        if (this.props.widget.DWG_ID.indexOf('TMP_') === 0) {
            params.type = this.state.type;
            request.post(
                'widget',
                params,
                () => {
                    this.props.getWidgets()
                    this.props.toggleModal();
                },
                () => console.log('create widget failed')
            )
        } else {
            request.patch(
                'widget/' + this.props.widget.DWG_ID,
                params,
                () => {
                    this.props.getWidgets();
                    this.props.toggleModal();
                },
                () => console.log('edit widget failed')
            );
        }
    }

    render() {
        const modal = this.props.modal;

        return (
            <div>
                <Modal onMouseDown={e => e.stopPropagation()} isOpen={modal} toggle={this.props.toggleModal} size="lg">
                    <ModalHeader toggle={this.props.toggleModal}>Edit {this.state.type} Widget - {this.state.label}</ModalHeader>
                    <ModalBody>
                        <Form>
                            <FormGroup>
                                <Row>
                                    <Col md={{size: 3}}>
                                        <Label className="label"> Widget Label </Label>
                                    </Col>
                                    <Col md={{size: 9}}>
                                        <Input
                                            valid={!!this.state.label.length}
                                            invalid={!this.state.label.length && this.state.labelTouched}
                                            type="text"
                                            name="label"
                                            value={this.state.label}
                                            onChange={this.handleInputChange}
                                        />
                                        <FormFeedback>Please enter a label for the widget.</FormFeedback>
                                    </Col>
                                </Row>
                            </FormGroup>
                            <FormGroup>
                                <Row>
                                    <Col md={{size: 3}}>
                                        <Label className="label">
                                            Refresh every (minutes)
                                        </Label>
                                    </Col>
                                    <Col md={{size: 9}}>
                                        <Input type="number" min="0" value={this.state.refreshFreq} name="refreshFreq" onChange={this.handleInputChange} />
                                    </Col>
                                </Row>
                            </FormGroup>
                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={this.edit}>Edit</Button>{' '}
                        <Button color="secondary" onClick={this.props.toggleModal}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Edit;
