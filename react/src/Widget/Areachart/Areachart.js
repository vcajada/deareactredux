import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';

import { request } from '../../lib/request.js';
import { utils } from '../../lib/utils.js';

import './Areachart.css';

class Areachart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            config: {},
            options: this.getDefaultOptions()
        };

        this.updateGraph = this.updateGraph.bind(this);
        this.setAreachartOptions = this.setAreachartOptions.bind(this);
    }

    componentDidMount() {
        if (!this.props.widget.CFG_IDS) { return; } // Widget hasn't been configured yet
        utils.setRefresh(this.props.widget.DWG_REFRESH_FREQ, this.updateGraph);
        this.updateGraph();
    }

    componentDidUpdate(prevProps) {
        if (
            prevProps.widget.CFG_IDS !== this.props.widget.CFG_IDS ||
            prevProps.widget.DWA_DATE_START !== this.props.widget.DWA_DATE_START ||
            prevProps.widget.DWA_DATE_STOP !== this.props.widget.DWA_DATE_STOP
        ) { this.updateGraph(); }
        if (utils.didWidgetResize(prevProps, this.props)) { this.refs.chart.getChart().reflow(); }
    }
    

    updateGraph() {

        let stateData = [];
        let stateConfig = [];
        let dataIndex = 0;
        let configIndex = 0;
        let configIdsArr = this.props.widget.CFG_IDS.split(',');

        configIdsArr.forEach((configId, i) => {
            request.get(
                'data/' + configId,
                {
                    max_data: 300,
                    date_start: this.props.widget.DWA_DATE_START,
                    date_stop: this.props.widget.DWA_DATE_STOP,
                    extrema: true
                },
                // success callback
                (data) => {
                    stateData['configId_' + configId] = data;

                },
                // fail callback
                () => console.log('load data failed'),
                // finish callback
                () => {
                    dataIndex ++;
                    // only render when all datas loaded
                    if (dataIndex === configIdsArr.length) {
                        this.setState({
                            data: stateData
                        }, () => {

                            // If config finished loading, then render, if not, let config do the render
                            if (configIndex === configIdsArr.length) {
                                this.setAreachartOptions();
                            }
                        });
                    }
                }
            );
            request.get(
                'config/' + configId,
                {},
                (config) => {
                    stateConfig['configId_' + configId] = config;
                },
                () => console.log('load config failed'),
                () => {
                    configIndex ++;
                    if (configIndex === configIdsArr.length) {
                        this.setState({
                            config: stateConfig
                        }, () => {

                            if (dataIndex === configIdsArr.length) {
                                this.setAreachartOptions();
                            }
                        });
                    }
                }
            )
        });
    }

    setAreachartOptions() {
        let series = Object.keys(this.state.config).map(key => {
            let datas = this.state.data[key];
            if (!datas) {
                return {
                    name: this.state.config[key][key.replace('configId_', '')].label_D1,
                    data: []
                };
            }

            delete datas['_links'];

            return {
                name: this.state.config[key][key.replace('configId_', '')].label_D1,
                data: Object.keys(datas).map(i => {
                    let value = datas[i].data;
                    let datetimeSplit = datas[i].date_creation.split(' ');
                    let dateSplit = datetimeSplit[0].split('-');
                    let timeSplit = datetimeSplit[1].split(':');
                    let date = Date.UTC(dateSplit[0], dateSplit[1], dateSplit[2], timeSplit[0], timeSplit[1], timeSplit[2]);

                    return [date, value];
                })
            }
        });

        let options = this.getDefaultOptions();
        options.series = series;
        this.setState({options: options});
    }

    getDefaultOptions() {

        return {
            chart: {
                type: 'areaspline',
                className: 'chart'
            },
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                title: {
                    text: 'Data'
                },
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                }
            },
            credits: {
                enabled: false
            }
        };
    }

    render() {

        return (
            <div className="highcharts-wrapper" style={{height: `${this.props.size.height}px`}}>
                <ReactHighcharts
                    isPureConfig
                    config={this.state.options}
                    ref="chart"
                />
            </div>
        )
    }
}

export default Areachart;
