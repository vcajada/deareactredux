import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import draftToHtml from 'draftjs-to-html';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import 'draft-js/dist/Draft.css';
import './Html.css';

class Html extends Component {
    constructor(props) {
        super(props);

        let text = this.getCurrentText();
        this.state = {
            editorType: 'textarea',
            // for draftjs we have to make a local copy of them editor state, because parent state is just converted html.
            // converted html doesnt remember the cursor position.
            editorState: text ? this.convertHtmlToDraftjs(text) : EditorState.createEmpty()
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleEditorStateChange = this.handleEditorStateChange.bind(this);
        this.convertHtmlToDraftjs = this.convertHtmlToDraftjs.bind(this);
        this.handleTextareaChange = this.handleTextareaChange.bind(this);
        this.getCurrentConfiguration = this.getCurrentConfiguration.bind(this);
        this.getCurrentText = this.getCurrentText.bind(this);
    }

    componentDidMount() {
        let currentConfiguration = this.getCurrentConfiguration();
        this.props.handleChange(currentConfiguration);
    }

    convertHtmlToDraftjs(html) {
        let contentText = '';
        const contentBlock = htmlToDraft(html);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            contentText = EditorState.createWithContent(contentState);
        }
        return contentText;
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleEditorStateChange(editorState) {

        let currentConfiguration = this.getCurrentConfiguration();
        currentConfiguration.text = draftToHtml(convertToRaw((editorState.getCurrentContent())));
        this.props.handleChange(currentConfiguration);

        this.setState({editorState: editorState});
    }

    handleTextareaChange(event) {

        let currentConfiguration = this.getCurrentConfiguration();
        currentConfiguration.text = event.target.value;
        this.props.handleChange(currentConfiguration);
    }

    getCurrentConfiguration() {
        return {
            type: 'html',
            text: this.getCurrentText(),
        }
    }

    getCurrentText() {
        return this.props.configuration.text ?
            this.props.configuration.text :
            (this.props.widget.DWH_TEXT ?
                this.props.widget.DWH_TEXT :
                ''
            );
    }

    render() {

        return (
            <Form>
                <FormGroup>
                    <Row>
                        <Col md={{size: 3}}>
                            <Label className="label">
                                Editor type
                            </Label>
                        </Col>
                        <Col md={{size: 9}}>
                            <Input type="select" name="editorType" value={this.state.editorType} onChange={this.handleInputChange}>
                                <option>textarea</option>
                                <option>draftjs</option>
                            </Input>
                        </Col>
                    </Row>
                </FormGroup>
                <FormGroup>
                    <Row>
                        <Col md={{size: 12}}>
                            <div className="editor-container">
                                {this.state.editorType === 'textarea' ? 
                                    <Input
                                        className="html-textarea"
                                        type="textarea"
                                        name="contentTextarea"
                                        value={this.getCurrentText()}
                                        onChange={this.handleTextareaChange}
                                    />
                                :
                                    <Editor
                                        editorState={this.state.editorState}
                                        onEditorStateChange={this.handleEditorStateChange}
                                        toolbarClassName="draftjs-toolbar"
                                        wrapperClassName="draftjs-wrapper"
                                        editorClassName="draftjs-editor"
                                    />
                                }
                            </div>
                        </Col>
                    </Row>
                </FormGroup>
            </Form>
        );
    }
}

export default Html;
