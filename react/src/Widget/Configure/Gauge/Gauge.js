import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';

import { request } from '../../../lib/request.js';

class Gauge extends Component {
    constructor(props) {
        super(props);

        this.state = {
            devices: [],
            configs: []
        };

        this.getConfigsOfDeviceId = this.getConfigsOfDeviceId.bind(this);
        this.handleDeviceChange = this.handleDeviceChange.bind(this);
        this.handleConfigChange = this.handleConfigChange.bind(this);
        this.getCurrentConfiguration = this.getCurrentConfiguration.bind(this);
        this.getCurrentDeviceId = this.getCurrentDeviceId.bind(this);
        this.getCurrentConfigId = this.getCurrentConfigId.bind(this);
    }

    componentDidMount() {
        // get devices
        request.get(
            'device',
            {},
            (devices) => this.setState({devices: devices}),
            () => console.log('load devices failed')
        );

        if (!!this.props.widget.DVC_ID && this.props.widget.DVC_ID.length) {
            this.getConfigsOfDeviceId(this.props.widget.DVC_ID);
        }

        let currentConfiguration = this.getCurrentConfiguration();
        this.props.handleChange(currentConfiguration);
    }

    // accepts string of comma separated device id
    getConfigsOfDeviceId(deviceId) {
        request.get(
            'config',
            {device_id: deviceId},
            (configs) => this.setState({configs: configs}),
            () => console.log('load configs failed')
        )
    }

    handleDeviceChange(event) {
        let currentConfiguration = this.getCurrentConfiguration();
        currentConfiguration.device_id = event.target.value;
        this.props.handleChange(currentConfiguration);

        this.getConfigsOfDeviceId(event.target.value);
    }

    handleConfigChange(event) {
        let currentConfiguration = this.getCurrentConfiguration();
        currentConfiguration.config_id = event.target.value;
        this.props.handleChange(currentConfiguration);
    }

    getCurrentConfiguration() {
        return {
            type: 'gauge',
            device_id: this.getCurrentDeviceId(),
            config_id: this.getCurrentConfigId()
        }
    }

    getCurrentDeviceId() {
        return this.props.configuration.device_id ? // if something is set in configuration, user has changed input values
                this.props.configuration.device_id : // configuration is the source of truth
                (this.props.widget.DVC_ID ? // if not check if current widget has device id, meaning we are editing an existing widget
                    this.props.widget.DVC_ID :
                    ''
                );
    }

    getCurrentConfigId() {
        return this.props.configuration.config_id ?
            this.props.configuration.config_id :
            (this.props.widget.CFG_ID ?
                this.props.widget.CFG_ID :
                ''
            );
    }

    render() {

        const currentDeviceId = this.getCurrentDeviceId();
        const currentConfigId = this.getCurrentConfigId();

        return (
            <Form>
                <FormGroup>
                    <Row>
                        <Col md={{size: 3}}>
                            <Label className="label">
                                Device
                            </Label>
                        </Col>
                        <Col md={{size: 9}}>
                            <Input type="select" name="selectedDevices" value={currentDeviceId} onChange={this.handleDeviceChange}>
                                { this.state.devices.map((device) =>
                                    <option key={device.id} value={device.id}>{device.label}</option>
                                )}
                            </Input>
                        </Col>
                    </Row>
                </FormGroup>
                <FormGroup>
                    {this.state.configs.length > 0 && <Row>
                        <Col md={{size: 3}}>
                            <Label className="label">
                                Datatypes
                            </Label>
                        </Col>
                        <Col md={{size: 9}}>
                            <Input type="select" name="selectedConfigs" value={currentConfigId} onChange={this.handleConfigChange}>
                                { this.state.configs.map((config) =>
                                    <option key={config.id} value={config.id}>{config.label_D1}</option>
                                )}
                            </Input>
                        </Col>
                    </Row>}
                </FormGroup>
            </Form>
        );
    }
}

export default Gauge;
