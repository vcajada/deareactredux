import React, { Component } from 'react';
import {
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button
} from 'reactstrap';

import Value from './Value/Value';
import Html from './Html/Html';
import Areachart from './Areachart/Areachart';
import Piechart from './Piechart/Piechart';
import Gauge from './Gauge/Gauge';
import Status from './Status/Status';
import Map from './Map/Map';
import Sitemap from './Sitemap/Sitemap';

import { request } from '../../lib/request';

class Configure extends Component {

    constructor(props) {
        super(props);
        this.state = {
            configuration: {}
        }

        this.configure = this.configure.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.onConfigurationChange = this.onConfigurationChange.bind(this);
    }

    configure() {
        let params = this.state.configuration;

        if (this.props.widget.DWG_ID.indexOf('TMP_') === 0) {
            params.label = this.props.widget.DWG_LABEL;
            params.page_id = this.props.dpgId
            request.post(
                'widget',
                params,
                () => {
                    this.props.getWidgets()
                    this.toggleModal();
                },
                () => console.log('create widget failed')
            )
        } else {
            request.patch(
                'widget/' + this.props.widget.DWG_ID,
                params,
                () => {
                    this.props.getWidgets();
                    this.toggleModal();
                },
                () => console.log('edit widget failed')
            );
        }
    }

    toggleModal() {
        this.setState({configuration: {}});
        this.props.toggleModal();
    }

    onConfigurationChange(configuration) {
        this.setState({configuration: configuration});
    }

    render() {
        return (
            <Modal onMouseDown={e => e.stopPropagation()} isOpen={this.props.modal} toggle={this.toggleModal} className={this.props.className} size="lg">
                <ModalHeader toggle={this.toggleModal}>Configure {this.props.widget.DWG_TYPE.replace('DWG_', '')} Widget - {this.props.widget.DWG_LABEL}</ModalHeader>
                <ModalBody>
                    { this.props.widget.DWG_TYPE === 'DWG_value' && <Value
                        handleChange={this.onConfigurationChange}
                        configuration={this.state.configuration}
                        widget={this.props.widget}
                    />}
                    { this.props.widget.DWG_TYPE === 'DWG_html' && <Html
                        handleChange={this.onConfigurationChange}
                        configuration={this.state.configuration}
                        widget={this.props.widget}
                    />}
                    { this.props.widget.DWG_TYPE === 'DWG_areachart' && <Areachart
                        handleChange={this.onConfigurationChange}
                        configuration={this.state.configuration}
                        widget={this.props.widget}
                    />}
                    { this.props.widget.DWG_TYPE === 'DWG_piechart' && <Piechart
                        handleChange={this.onConfigurationChange}
                        configuration={this.state.configuration}
                        widget={this.props.widget}
                    />}
                    { this.props.widget.DWG_TYPE === 'DWG_gauge' && <Gauge
                        handleChange={this.onConfigurationChange}
                        configuration={this.state.configuration}
                        widget={this.props.widget}
                    />}
                    { this.props.widget.DWG_TYPE === 'DWG_status' && <Status
                        handleChange={this.onConfigurationChange}
                        configuration={this.state.configuration}
                        widget={this.props.widget}
                    />}
                    { this.props.widget.DWG_TYPE === 'DWG_map' && <Map
                        handleChange={this.onConfigurationChange}
                        configuration={this.state.configuration}
                        widget={this.props.widget}
                    />}
                    { this.props.widget.DWG_TYPE === 'DWG_sitemap' && <Sitemap
                        handleChange={this.onConfigurationChange}
                        configuration={this.state.configuration}
                        widget={this.props.widget}
                    />}
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.configure}>Configure</Button>{' '}
                    <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

export default Configure;
