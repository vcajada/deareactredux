import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';

import { request } from '../../../lib/request.js';
import { utils } from '../../../lib/utils.js';

class Status extends Component {

    constructor(props) {
        super(props);
        this.state = {
            devices: []
        }

        this.handleDeviceChange = this.handleDeviceChange.bind(this);
        this.getCurrentConfiguration = this.getCurrentConfiguration.bind(this);
        this.getCurrentDeviceIds = this.getCurrentDeviceIds.bind(this);
    }

    componentDidMount() {
        request.get(
            'device',
            {},
            (devices) => this.setState({devices: devices}),
            () => console.log('load devices failed')
        );

        let currentConfiguration = this.getCurrentConfiguration();
        this.props.handleChange(currentConfiguration);
    }

    handleDeviceChange(event) {
        let deviceIdsStr = utils.getSelectedOptionsFromEvent(event);

        let currentConfiguration = this.getCurrentConfiguration();
        currentConfiguration.device_ids = deviceIdsStr;
        this.props.handleChange(currentConfiguration);
    }

    getCurrentConfiguration() {
        return {
            type: 'map',
            device_ids: this.getCurrentDeviceIds()
        }
    }

    getCurrentDeviceIds() {
        return this.props.configuration.device_ids ? // if something is set in configuration, user has changed input values
                this.props.configuration.device_ids : // configuration is the source of truth
                (this.props.widget.DVC_IDS ? // if not check if current widget has device ids, meaning we are editing an existing widget
                    this.props.widget.DVC_IDS :
                    ''
                );
    }

    render() {

        const currentDeviceIds = this.getCurrentDeviceIds().split(',');

        return (
            <Form>
                <FormGroup>
                    <Row>
                        <Col md={{size: 3}}>
                            <Label className="label">
                                Device
                            </Label>
                        </Col>
                        <Col md={{size: 9}}>
                            <Input type="select" multiple name="device" value={currentDeviceIds} onChange={this.handleDeviceChange}>
                                <option value="">Select a device</option>
                                { this.state.devices.map((device) =>
                                    <option key={device.id} value={device.id}>{device.label}</option>
                                )}
                            </Input>
                        </Col>
                    </Row>
                </FormGroup>
            </Form>
        );
    }
}

export default Status;
