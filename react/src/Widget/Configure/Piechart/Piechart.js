import React, { Component } from 'react';
import { Row, Col, Form, FormGroup, Label, Input } from 'reactstrap';

import { request } from '../../../lib/request.js';
import { utils } from '../../../lib/utils.js';

class Piechart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            devices: [],
            configs: []
        };

        this.getConfigsOfDeviceIds = this.getConfigsOfDeviceIds.bind(this);
        this.handleDeviceChange = this.handleDeviceChange.bind(this);
        this.handleConfigChange = this.handleConfigChange.bind(this);
        this.getCurrentConfiguration = this.getCurrentConfiguration.bind(this);
        this.getCurrentDeviceIds = this.getCurrentDeviceIds.bind(this);
        this.getCurrentConfigIds = this.getCurrentConfigIds.bind(this);
    }

    componentDidMount() {
        // get devices
        request.get(
            'device',
            {},
            (devices) => this.setState({devices: devices}),
            () => console.log('load devices failed')
        );

        if (!!this.props.widget.DVC_IDS && this.props.widget.DVC_IDS.length) {
            this.getConfigsOfDeviceIds(this.props.widget.DVC_IDS);
        }

        let currentConfiguration = this.getCurrentConfiguration();
        this.props.handleChange(currentConfiguration);
    }

    // accepts string of comma separated device ids
    getConfigsOfDeviceIds(deviceIds) {
        request.get(
            'config',
            {device_id: deviceIds},
            (configs) => this.setState({configs: configs}),
            () => console.log('load configs failed')
        )
    }

    handleDeviceChange(event) {
        let deviceIdsStr = utils.getSelectedOptionsFromEvent(event);

        let currentConfiguration = this.getCurrentConfiguration();
        currentConfiguration.device_ids = deviceIdsStr;
        this.props.handleChange(currentConfiguration);

        this.getConfigsOfDeviceIds(deviceIdsStr);
    }

    handleConfigChange(event) {
        let configIdsStr = utils.getSelectedOptionsFromEvent(event);

        let currentConfiguration = this.getCurrentConfiguration();
        currentConfiguration.config_ids = configIdsStr;
        this.props.handleChange(currentConfiguration);
    }

    getCurrentConfiguration() {
        return {
            type: 'piechart',
            device_ids: this.getCurrentDeviceIds(),
            config_ids: this.getCurrentConfigIds()
        }
    }

    getCurrentDeviceIds() {
        return this.props.configuration.device_ids ? // if something is set in configuration, user has changed input values
                this.props.configuration.device_ids : // configuration is the source of truth
                (this.props.widget.DVC_IDS ? // if not check if current widget has device ids, meaning we are editing an existing widget
                    this.props.widget.DVC_IDS :
                    ''
                );
    }

    getCurrentConfigIds() {
        return this.props.configuration.config_ids ?
            this.props.configuration.config_ids :
            (this.props.widget.CFG_IDS ?
                this.props.widget.CFG_IDS :
                ''
            );
    }

    render() {

        const currentDeviceIds = this.getCurrentDeviceIds().split(',');
        const currentConfigIds = this.getCurrentConfigIds().split(',');

        return (
            <Form>
                <FormGroup>
                    <Row>
                        <Col md={{size: 3}}>
                            <Label className="label">
                                Device
                            </Label>
                        </Col>
                        <Col md={{size: 9}}>
                            <Input type="select" multiple name="selectedDevices" value={currentDeviceIds} onChange={this.handleDeviceChange}>
                                { this.state.devices.map((device) =>
                                    <option key={device.id} value={device.id}>{device.label}</option>
                                )}
                            </Input>
                        </Col>
                    </Row>
                </FormGroup>
                <FormGroup>
                    {this.state.configs.length > 0 && <Row>
                        <Col md={{size: 3}}>
                            <Label className="label">
                                Datatypes
                            </Label>
                        </Col>
                        <Col md={{size: 9}}>
                            <Input type="select" name="selectedConfigs" multiple value={currentConfigIds} onChange={this.handleConfigChange}>
                                { this.state.configs.map((config) =>
                                    <option key={config.id} value={config.id}>{config.device.label} - {config.label_D1}</option>
                                )}
                            </Input>
                        </Col>
                    </Row>}
                </FormGroup>
            </Form>
        );
    }
}

export default Piechart;
