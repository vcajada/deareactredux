import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import { fitBounds } from 'google-map-react/utils';

import Marker from './Marker/Marker';

import { request } from '../../lib/request.js';
import { utils } from '../../lib/utils.js';
import config from '../../config/config';

class Map extends Component {
    constructor(props) {
        super(props);

        this.state = {
            geoloc: [],
            center: {
                lat: 59.95,
                lng: 30.33
            },
            zoom: 8,
        };

        this.getDevicesGeoloc = this.getDevicesGeoloc.bind(this);
        this.setBounds = this.setBounds.bind(this);
    }

    componentDidMount() {
        if (!this.props.widget.DVC_IDS) { return; }
        utils.setRefresh(this.props.widget.DWG_REFRESH_FREQ, this.getDevicesGeoloc);
        if (this.props.widget.DVC_IDS) { this.getDevicesGeoloc(); }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.widget.DVC_IDS !== this.props.widget.DVC_IDS) {
            this.getDevicesGeoloc();
        }
    }

    getDevicesGeoloc() {
        this.props.widget.DVC_IDS.split(',').forEach((deviceId) => {
            if (!deviceId) return;
            request.get(
                'geoloc',
                {device_id: deviceId},
                (geoloc) => {
                    if (!geoloc.length) { return; }
                    this.setState({
                        geoloc: [...this.state.geoloc, geoloc[0]]
                    }, () => this.setBounds());
                },
                () => console.log('load device failed')
            );
        });
    }

    setBounds() {
        if (this.state.geoloc.length === 1) {
            this.setState({
                center: {lat: parseFloat(this.state.geoloc[0].latitude), lng: parseFloat(this.state.geoloc[0].longitude)},
                zoom: 10
            })
            return;
        }
        if (!this.state.geoloc.length) {
            return;
        }
        let bounds = {
            ne: {
                lat: null,
                lng: null
            },
            sw: {
                lat: null,
                lng: null
            }
        }
        this.state.geoloc.forEach(loc => {
            if (bounds.ne.lat === null || bounds.ne.lat < loc.latitude) { bounds.ne.lat = loc.latitude; }
            if (bounds.ne.lng === null || bounds.ne.lng > loc.longitude) { bounds.ne.lng = loc.longitude; }
            if (bounds.sw.lat === null || bounds.sw.lat > loc.latitude) { bounds.sw.lat = loc.latitude; }
            if (bounds.sw.lng === null || bounds.sw.lng < loc.longitude) { bounds.sw.lng = loc.longitude; }
        });
        const {center, zoom} = Object.keys(bounds).length > 0 ?
            fitBounds(bounds, {width: this.props.size.width, height: this.props.size.height}) :
            this.state;

        this.setState({center: center, zoom: zoom});
    }

    render() {

        return (
            <div style={{width: this.props.size.width, height: this.props.size.height}} onMouseDown={e => e.stopPropagation()}>
                {Object.keys(this.state.geoloc).length > 0 &&
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: config.gmaps.key }}
                        center={this.state.center}
                        zoom={this.state.zoom}
                        yesIWantToUseGoogleMapApiInternals
                    >
                        {this.state.geoloc.map(loc => <Marker
                            key={loc.id}
                            lat={loc.latitude}
                            lng={loc.longitude}
                            text={loc.device.label}
                        />)}
                    </GoogleMapReact>
                }
            </div>
        )
    }
}

export default Map;
