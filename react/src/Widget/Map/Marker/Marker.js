import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { withSize } from 'react-sizeme';

import './Marker.css';

class Marker extends Component {

    render() {
        return (
            <FontAwesomeIcon
                icon="map-marker"
                title={this.props.text}
                className="map-marker"
                style={{top: `-${this.props.size.height}px`, left: `-${this.props.size.width/2}px`}}
            />
        )
    }
}

export default withSize({monitorHeight: true})(Marker);
