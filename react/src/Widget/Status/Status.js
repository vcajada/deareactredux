import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    Row,
    Col
} from 'reactstrap';

import { request } from '../../lib/request.js';
import { utils } from '../../lib/utils.js';

import './Status.css';

class Value extends Component {
    constructor(props) {
        super(props);

        this.state = {
            config: {},
            device: {},
        };

        this.getDeviceLastData = this.getDeviceLastData.bind(this);
    }

    componentDidMount() {
        if (!this.props.widget.DVC_ID) { return; }
        utils.setRefresh(this.props.widget.DWG_REFRESH_FREQ, this.getDeviceLastData);
        this.getDeviceLastData();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.widget.DVC_ID !== this.props.widget.DVC_ID || prevProps.widget.CFG_ID !== this.props.widget.CFG_ID) {
            this.getDeviceLastData();
        }
    }

    getDeviceLastData() {
        request.get(
            'device/' + this.props.widget.DVC_ID,
            {},
            (device) => {
                let config = this.getCurrentConfigOfDevice(device);
                this.setState({device: device[0], config: config});
            },
            () => console.log('load device failed')
        );
    }

    getCurrentConfigOfDevice(device) {
        let configs = device[0].configs;
        for (let i = 0; i < configs.length; i++) {
            if (configs[i].id === this.props.widget.CFG_ID) {
                return configs[i];
            }
        }
    }

    render() {
        return (
            <div>
                {Object.keys(this.state.config).length > 0 &&
                    <Row>
                        <Col md={{size: 7}}>
                            <div className="status-text">
                                <div>{this.state.device.label}</div>
                                <div>{this.state.config.label}</div>
                            </div>
                        </Col>
                        <Col md={{size: 5}}>
                            <FontAwesomeIcon
                                icon={this.state.config.value ? 'toggle-on' : 'toggle-off'}
                                className="status-icon"
                            />
                        </Col>
                    </Row>
                }
            </div>
        )
    }
}

export default Value;
