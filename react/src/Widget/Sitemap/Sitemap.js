import React, { Component } from 'react';

import { request } from '../../lib/request.js';
import { utils } from '../../lib/utils.js';

class Sitemap extends Component {
    constructor(props) {
        super(props);

        this.state = {
            config: {},
            device: {},
        };

        this.getDeviceLastData = this.getDeviceLastData.bind(this);
    }

    componentDidMount() {
        if (!this.props.widget.DVC_ID) { return; }
        utils.setRefresh(this.props.widget.DWG_REFRESH_FREQ, this.getDeviceLastData);
        this.getDeviceLastData();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.widget.DVC_ID !== this.props.widget.DVC_ID) {
            this.getDeviceLastData();
        }
    }

    getDeviceLastData() {
        request.get(
            'device/' + this.props.widget.DVC_ID,
            {},
            (device) => {
                let config = this.getCurrentConfigOfDevice(device);
                this.setState({device: device[0], config: config});
            },
            () => console.log('load device failed')
        );
    }

    getCurrentConfigOfDevice(device) {
        let configs = device[0].configs;
        for (let i = 0; i < configs.length; i++) {
            if (configs[i].id === this.props.widget.CFG_ID) {
                return configs[i];
            }
        }
    }

    render() {
        return (
            <div className="value-container" style={{height: `${this.props.size.height}px`}}>
                <div className="value-content">
                    <div className="title">
                        {this.state.device.label}
                    </div>
                    <div className="label">
                        {this.state.config.label}
                    </div>
                    <div className="value">
                        {this.state.config.value} {this.state.config.unite_D1}
                    </div>
                    <div className="date">
                        {this.state.config.date}
                    </div>
                </div>
            </div>
        )
    }
}

export default Sitemap;
