import React, { Component } from 'react';
import DOMPurify from 'dompurify';
import ReactMarkdown from 'react-markdown';

import './Html.css';

            /*dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(this.state.body)}}*/
class Html extends Component {
    render() {
        return (
            <ReactMarkdown
                source={DOMPurify.sanitize(this.props.widget.DWH_TEXT)}
                escapeHtml={false}
            />
        )
    }
}

export default Html;
