import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';

import { request } from '../../lib/request.js';
import { utils } from '../../lib/utils.js';

import './Piechart.css';

class Piechart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            datas: [],
            config: [],
            options: this.getDefaultOptions()
        };

    }

    componentDidMount() {
        if (!this.props.widget.CFG_IDS) { return; } // Widget hasn't been configured yet
        utils.setRefresh(this.props.widget.DWG_REFRESH_FREQ, this.updateGraph);
        this.updateGraph();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.widget.CFG_IDS !== this.props.widget.CFG_IDS) { this.updateGraph(); }
        if (utils.didWidgetResize(prevProps, this.props)) { this.refs.chart.getChart().reflow(); }
    }

    updateGraph() {

        let stateDatas = [];
        let stateConfigs = [];
        let dataIndex = 0;
        let configIndex = 0;
        let configIdsArr = this.props.widget.CFG_IDS.split(',');

        configIdsArr.forEach((configId, i) => {
            request.get(
                'data/' + configId,
                {
                    data_type: 'last_data'
                },
                // success callback
                (data) => {
                    stateDatas['configId_' + configId] = data;
                },
                // fail callback
                () => console.log('load data failed'),
                // finish callback
                () => {
                    dataIndex ++;
                    // only render when all datas loaded
                    if (dataIndex === configIdsArr.length) {
                        this.setState({
                            datas: stateDatas
                        }, () => {

                            // If config finished loading, then render, if not, let config do the render
                            if (configIndex === configIdsArr.length) {
                                this.setPiechartOptions();
                            }
                        });
                    }
                }
            );
            request.get(
                'config/' + configId,
                {},
                (config) => {
                    stateConfigs['configId_' + configId] = config;
                },
                () => console.log('load config failed'),
                () => {
                    configIndex ++;
                    if (configIndex === configIdsArr.length) {
                        this.setState({
                            configs: stateConfigs
                        }, () => {

                            if (dataIndex === configIdsArr.length) {
                                this.setPiechartOptions();
                            }
                        });
                    }
                }
            )
        });
    }

    setPiechartOptions() {
        let data = Object.keys(this.state.configs).map(key => {
            let datas = this.state.datas[key];
            if (!datas) {
                return {
                    name: this.state.configs[key][key.replace('configId_', '')].label_D1,
                    data: {
                        y: 0
                    }
                };
            }

            delete datas['_links'];

            return {
                name: this.state.configs[key][key.replace('configId_', '')].label_D1,
                y: datas[0].data
            }
        });

        let series = [{ data: data }];
        let options = this.getDefaultOptions();
        options.series = series;
        this.setState({options: options});
    }

    getDefaultOptions() {

        return {
            chart: {
                type: 'pie',
                className: 'chart'
            },
            title: {
                text: ''
            },
            tooltip: {
                headerFormat: '<b>{series.name}</b><br>',
                pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    }
                }
            },
            credits: {
                enabled: false
            }
        };
    }

    render() {

        return (
            <div className="highcharts-wrapper" style={{height: `${this.props.size.height}px`}}>
                <ReactHighcharts
                    config={this.state.options}
                    isPureConfig
                    ref="chart"
                />
            </div>
        )
    }
}
export default Piechart;
