import React, { Component } from 'react';
import {
    Button,
    Card,
    CardBody,
    CardTitle,
    Form,
    FormGroup,
    Label,
    Input,
    Container,
    Row,
    Col
} from 'reactstrap';
import { Redirect } from 'react-router-dom';

import { auth } from '../lib/auth.js';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false,
            username: '',
            password: '',
            errorMessage: ''
        }

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    login(e) {
        e.preventDefault();
        auth.authenticate(
            this.state.username,
            this.state.password,
            () => {
                this.setState(() => ({
                    redirectToReferrer: true
                }));
            },
            () => {
                this.setState({
                    errorMessage: 'Invalid username and / or password.'
                });
            }
        );
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        const from = this.props.location.state ? this.props.location.state.from : { pathname: '/'+this.props.match.params.dap_id };
        const redirectToReferrer = this.state.redirectToReferrer;

        if (redirectToReferrer) {
            return <Redirect to={from} />;
        }

        return (
            <Container fluid>
                <Row>
                    <Col md={{ size: 4, offset: 4 }}>
                        <Card>
                            <CardBody>
                                <CardTitle>
                                    Login
                                </CardTitle>
                                <Form onSubmit={(e) => this.login(e)}>
                                    <FormGroup>
                                        <Label for="username">
                                            Username
                                        </Label>
                                        <Input type="text" id="username" name="username" value={this.state.username} onChange={this.handleInputChange} required />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="password">
                                            Password
                                        </Label>
                                        <Input type="password" id="password" name="password" value={this.state.password} onChange={this.handleInputChange} required />
                                    </FormGroup>
                                    <Button>Login</Button>
                                </Form>
                                <div>{this.state.errorMessage}</div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Login;
