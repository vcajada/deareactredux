import React, { Component } from 'react';
import { Responsive, WidthProvider } from 'react-grid-layout';
import { Card } from 'reactstrap';

import Header from '../Header/Header'
import Widget from '../Widget/Widget';
import Toolbar from '../Toolbar/Toolbar';

import { auth } from '../lib/auth';
import { request } from '../lib/request.js';
import { utils } from '../lib/utils.js';

import 'react-grid-layout/css/styles.css'
import 'react-resizable/css/styles.css'

import './Dashboard.css';

const ResponsiveGridLayout = WidthProvider(Responsive);

class Dashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            widgets: [],
            currentPage: {},
            layouts: {}
        };

        this.getWidgets = this.getWidgets.bind(this);
        this.loadPage = this.loadPage.bind(this);
        this.onBreakpointChange = this.onBreakpointChange.bind(this);
        this.onLayoutChange = this.onLayoutChange.bind(this);
        this.addWidget = this.addWidget.bind(this);
    }

    componentDidMount() {
        this.getWidgets(this.loadPage);
        //this.loadPage(this.props.match.params.dpg_id);
    }

    componentWillReceiveProps(newProps) {
        this.getWidgets(this.loadPage);
        //this.loadPage(newProps.match.params.dpg_id);
    }

    loadPage(dpgId) {
        request.get(
            'page/' + dpgId,
            {},
            (page) => {
                this.setState({
                    currentPage: page,
                    layouts: utils.isValidJson(page.DPG_LAYOUT) ? JSON.parse(page.DPG_LAYOUT) : {}
                });
            },
            () => { console.log('load page failed'); }
        )
    }

    getWidgets(cb) {
        let dpgId = this.props.match.params.dpg_id;
        request.get(
            'widget',
            {page_id: dpgId},
            (widgets) => {
                this.setState({ widgets: widgets }, () => {if (typeof cb !== 'undefined') { cb(dpgId); }});
            },
            () => { console.log('load widgets failed'); }
        );
    }

    onLayoutChange(layout, layouts) {

        this.setState({layouts: layouts});

        request.patch(
            'page/' + this.props.match.params.dpg_id,
            {
                layout: layouts
            },
            () => {
                let currentPage = this.state.currentPage;
                currentPage.DPG_LAYOUT = layouts;
                this.setState({currentPage: currentPage});
            },
            () => console.log('layout save failed')
        )
    }

    // We're using the cols coming back from this to calculate where to add new items.
    onBreakpointChange(breakpoint, cols) {
        this.setState({
            breakpoint: breakpoint,
            cols: cols
        });
    }

    addWidget(type) {
        this.setState(prevState => ({ widgets: [...prevState.widgets, {DWG_LABEL: 'New widget', DWG_TYPE: type, DWG_ID: 'TMP_' + (new Date()).getTime()}] }))
    }

    render() {

        return (
            <div>
                <Header show={auth.isAuthenticated} dapId={this.props.match.params.dap_id} dpgId={this.props.match.params.dpg_id}/>
                <Toolbar
                    dpgId={this.props.match.params.dpg_id}
                    getWidgets={this.getWidgets}
                    addWidget={this.addWidget}
                />
                <div className="content">
                    <div className="dashboard-container">
                        <ResponsiveGridLayout
                            breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}} cols={{lg: 12, md: 10, sm: 6, xs: 4, xxs: 2}}
                            layouts={this.state.layouts}
                            onBreakpointChange={this.onBreakpointChange}
                            onLayoutChange={(layout, layouts) =>
                                this.onLayoutChange(layout, layouts)
                            }
                        >
                            {this.state.widgets.map((widget) =>
                                <Card key={widget.DWG_ID} className="widget-container">
                                    <Widget
                                        widget={widget}
                                        getWidgets={this.getWidgets}
                                        layouts={this.state.layouts}
                                        dpgId={this.props.match.params.dpg_id}
                                    />
                                </Card>
                            )}
                        </ResponsiveGridLayout>
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard;
