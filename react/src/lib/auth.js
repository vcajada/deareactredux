import config from '../config/config';

export const auth = {
    isAuthenticated: (localStorage.getItem('token') !== null && localStorage.getItem('token') !== '' ? true : false),
    authenticate(username, password, cb_success, cb_fail) {
        let baseUrl = config.apiUrl + 'token';
        let url = baseUrl + '?login=' + username + '&password=' + password;

        fetch(url)
            .then((response) => {
                if (response.status !== 200) {
                    return { fail: true };
                }
                return response.json();
            })
            .then((responseJson) => {
                if (responseJson.fail === true) {
                    cb_fail();
                    return;
                }
                localStorage.setItem('token', responseJson['_embedded'].token[0].token);
                this.isAuthenticated = true;
                cb_success();
            });
    },
    signout(cb) {
        localStorage.removeItem('token');
        this.isAuthenticated = false;
        cb();
    }
};

