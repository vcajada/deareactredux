import config from '../config/config_dev';
import history from './history'
import { matchPath } from 'react-router'

export const request = {
    get(resource, params, cb_success, cb_fail, cb_finish) {
        let baseUrl = config.apiUrl + resource;
        let url = baseUrl + '?token=' + localStorage.getItem('token') + '&' + Object.keys(params).map(k => k + '=' + params[k]).join('&');

        fetch(url)
        .then((response) => {
            if (response.status === 422) {
                const { params }= matchPath(window.location.pathname, {
                    path: "/:dap_id"
                });
                localStorage.removeItem('token');
                history.push('/' + params.dap_id + '/login');
            }
            if (response.status !== 200) {
                return { fail: true };
            }
            return response.json();
        }).then((responseJson) => {
            if (responseJson.fail === true) {
                cb_fail();
            } else {
                cb_success(typeof responseJson['_embedded'] === 'undefined' ? responseJson : responseJson['_embedded'][resource]);
            }
            if (typeof cb_finish !== 'undefined') { cb_finish(); }
        });
    },
    post(resource, params, cb_success, cb_fail, cb_finish) {
        this.genericRequest(resource, params, cb_success, cb_fail, cb_finish, 'POST');
    },
    patch(resource, params, cb_success, cb_fail, cb_finish) {
        this.genericRequest(resource, params, cb_success, cb_fail, cb_finish, 'PATCH');
    },
    delete(resource, params, cb_success, cb_fail, cb_finish) {
        this.genericRequest(resource, params, cb_success, cb_fail, cb_finish, 'DELETE');
    },
    genericRequest(resource, params, cb_success, cb_fail, cb_finish, method) {
        let baseUrl = config.apiUrl + resource;
        let url = baseUrl + '?token=' + localStorage.getItem('token');

        fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(params)
        }).then((response) => {
            if (response.status !== 200) {
                return { fail: true };
            }
            return response.json();
        }).then((responseJson) => {
            if (responseJson.fail === true) {
                cb_fail();
            } else {
                cb_success(responseJson);
            }
            if (typeof cb_finish !== 'undefined') {
                cb_finish();
            }
        });
    }
}
