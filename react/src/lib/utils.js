export const utils = {
    didWidgetResize(prevProps, props) {
        /* old more expensive way. before react-sizeme
        if (prevProps.size.height !== props.size.height || prevProps.size.width !== props.size.width) {
            return true;
        }
        return false;
        */

        let result = false;
        Object.keys(prevProps.layouts).forEach((key) => {
            prevProps.layouts[key].forEach((item, subkey) => {
                if (
                    typeof props.layouts[key] === 'undefined' ||
                    item.i !== props.widget.DWG_ID
                ) { return false; }

                let stateItem = props.layouts[key][subkey];
                if (typeof stateItem === 'undefined' || typeof item === 'undefined') { return false; }
                if(stateItem.w !== item.w || stateItem.h !== item.h) { result = true; }
            });
        });

        return result;
    },
    getSelectedOptionsFromEvent(event) {
        let options = event.target.options;
        let selectedOptionsStr = '';
        for (let i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                selectedOptionsStr += options[i].value
                selectedOptionsStr += ',';
            }
        }

        selectedOptionsStr = selectedOptionsStr.slice(0, -1);
        return selectedOptionsStr;
    },
    isValidJson(string) {
        try {
            JSON.parse(string);
        } catch(e) {
            return false;
        }
        return true;
    },
    setRefresh(refreshFreq, cb) {
        if (parseInt(refreshFreq) !== 0) {
            setInterval(cb, refreshFreq *1000*60); // refresh freq is in minutes
        }
    }
};
