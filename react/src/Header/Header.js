import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    Navbar,
    NavbarBrand,
    NavbarToggler,
    Collapse,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import { NavLink as RrNavLink } from 'react-router-dom';
import { request } from '../lib/request.js';

import './Header.css';

import LogoutLink from '../LogoutLink/LogoutLink';

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            pages: []
        };

        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        request.get(
            'page',
            {application_id: this.props.dapId},
            (pages) => {
                this.setState({ pages: pages });
            },
            () => { console.log('load pages failed'); }
        );
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        if (!this.props.show) {
            return null;
        }

        return (
            <Navbar expand="md" dark className="navbar-container">
                <NavbarBrand href="/" className="logo-container">
                    <FontAwesomeIcon className="icon" icon="home" />
                    <span className="logo-cs"> CommonSense</span>
                </NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        {this.state.pages.map((page) =>
                            <NavItem key={page.DPG_ID}>
                                <NavLink
                                    tag={RrNavLink}
                                    to={"/" + this.props.dapId + "/dashboard/" + page.DPG_ID}
                                    exact
                                    activeClassName="active"
                                    className="header-link"
                                >{page.DPG_LABEL}</NavLink>
                            </NavItem>
                        )}
                        <NavItem>
                            <LogoutLink dapId={this.props.dapId} />
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        );
    }
}

export default Header;
