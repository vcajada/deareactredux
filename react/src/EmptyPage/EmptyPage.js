import React, { Component } from 'react';
import Header from '../Header/Header'
import { CardTitle } from 'reactstrap';
import './EmptyPage.css';

import { request } from '../lib/request';
import { auth } from '../lib/auth';
import history from '../lib/history';

class EmptyPage extends Component {

    componentDidMount() {
        request.get(
            'page',
            {application_id: this.props.match.params.dap_id},
            (pages) => {
                history.push('/' + this.props.match.params.dap_id + '/dashboard/' + pages[0].DPG_ID);
            },
            () => { console.log('load pages failed'); }
        );
    }

    render() {
        return(
            <div>
                <Header show={auth.isAuthenticated} dapId={this.props.match.params.dap_id}/>
                <div className="content">
                    <CardTitle className="text-center title">
                        Empty page. Do stuff.
                    </CardTitle>
                </div>
            </div>
        );
    }
}

export default EmptyPage;
