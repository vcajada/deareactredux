import React from "react";
import { render } from "react-dom";
import { Router } from 'react-router';

import * as serviceWorker from './serviceWorker';

import App from "./App/App";

import history from './lib/history';

import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

render(
    <Router history={history}>
        <App />
    </Router>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
