import React, { Component } from 'react';
import { CardTitle } from 'reactstrap';

class NotFound extends Component {

    render() {
        return(
            <div className="content">
                <CardTitle className="text-center title">
                    404 - Page not found
                </CardTitle>
            </div>
        );
    }
}

export default NotFound;
